
file = open('wiki.txt', 'r')
file_new = open('wiki_neu.txt', 'w')
linkstart = '[['
linkend = ']]'
new_file = []
flag = False

# insert head
file_new.write('{| class="wikitable sortable"')
file_new.write('|+')
file_new.write('!')
file_new.write('!')

# insert wiki-intern-links
for line in file:
    # print('pre: ', line)
    index = line.find('|')
    if flag == False:
        if ('|[[' not in line) and ('|-' not in line) and (line != '|'):
            line = line[:index+1] + linkstart + line[index+1:]
            line = line[:-1] + linkend + '\n'
            flag = True
    else:
        flag = False
    file_new.write(line)

file_new.write('}')

file_new.close()
file.close()